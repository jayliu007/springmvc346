package com.iweb.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author jay
 * @date 2023/3/26
 * @description
 */
@Data
@NoArgsConstructor
public class User {
    public User(Integer id, String loginName, String userName) {
        this.id = id;
        this.loginName = loginName;
        this.userName = userName;
    }

    private Integer id;
    private String loginName;//登录名称
    private String userName;//用户名
    private String password;//密码
    private Integer sex;//性别
    private String identityCode;
    private String email;//电子邮箱
    private String mobile;//电话
    private Integer type;//用户类别
    /**
     * 出生日期
     */
    // 处理key-value方式的同步表单提交的日期格式,只能单向(入参)格式化
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    // 处理json格式提交的数据中的日期格式 实现双向参数的格式化
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date bornDate;
}
