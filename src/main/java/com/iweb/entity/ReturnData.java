package com.iweb.entity;

/**
 * @author jay
 * @date 2023/3/30
 * @description 通过的异步请求响应数据的结构体
 */
public class ReturnData {
    private String code;
    private Object data;
    private String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static ReturnData success() {
        ReturnData returnData = new ReturnData();
        returnData.code = "200";
        return returnData;
    }

    public static ReturnData success(Object data) {
        ReturnData returnData = ReturnData.success();
        returnData.data = data;
        return returnData;
    }

    public static ReturnData success(Object data, String message) {
        ReturnData returnData = ReturnData.success();
        returnData.data = data;
        returnData.message = message;
        return returnData;
    }

    public static ReturnData error() {
        ReturnData returnData = new ReturnData();
        returnData.code = "500";
        return returnData;
    }

    public static ReturnData error(String message) {
        ReturnData returnData = ReturnData.error();
        returnData.message = message;
        return returnData;
    }
}
