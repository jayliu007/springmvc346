package com.iweb.entity;

import lombok.Data;

/**
 * @author jay
 * @date 2023/4/14
 * @description
 */
@Data
public class Product {
    private String name;
    private Double price;
    private Integer stock;
    private String fileName;
}
