package com.iweb.controller;

import com.iweb.entity.ReturnData;
import com.iweb.entity.User;
import com.iweb.exception.MyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * @author jay
 * @date 2023/4/13
 * @description 用户业务对应的前端控制器
 */
@Controller
@RequestMapping("user")
public class UserController {

    /**
     * rest风格   /user/1
     *
     * @param id
     * @return
     */
    @ResponseBody
    @GetMapping("{id}")
    public ReturnData detail5(@PathVariable Integer id) {
        User user = new User(id, "tom", "汤姆");
        return ReturnData.success(user, "查询成功");
    }

    /**
     * rest风格   /user/1
     * 删除
     *
     * @param id
     * @return
     */
    @ResponseBody
    @DeleteMapping("{id}")
    public ReturnData remove(@PathVariable Integer id) {
        User user = new User(id, "tom", "汤姆");
        return ReturnData.success(user, "删除成功");
    }

    /**
     * 处理查询用户列表
     */
    @RequestMapping(value = {"list"})
    public ModelAndView list(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        // 查询数据-模拟查询到的数据
        List<User> userList = new ArrayList<>();
        userList.add(new User(1, "tom1", "汤姆1"));
        userList.add(new User(2, "tom2", "汤姆2"));
        userList.add(new User(3, "tom23", "汤姆3"));
        // 原始做法，把userList保存到request中，然后转发响应到userList.jsp
        ModelAndView mv = new ModelAndView();
        // 保存对象数据到request中
        mv.addObject("userList", userList);
        // 设置逻辑视图名
        mv.setViewName("userList");

        return mv;
    }

    /**
     * 处理查询用户列表-分页
     */
    @RequestMapping(value = {"pageList"})
    public ModelAndView pageList(@RequestParam(name = "currentPage", required = false, defaultValue = "1") Integer pageIndex,
                                 @RequestParam(required = false, defaultValue = "5") Integer pageSize) throws Exception {
        // 查询数据-模拟查询到的数据
        List<User> userList = new ArrayList<>();
        userList.add(new User(1, "tom1", "汤姆1"));
        userList.add(new User(2, "tom2", "汤姆2"));
        userList.add(new User(3, "tom23", "汤姆3"));

        // 原始做法，把userList保存到request中，然后转发响应到userList.jsp
        ModelAndView mv = new ModelAndView();
        // 保存对象数据到request中
        mv.addObject("userList", userList);

        // 设置逻辑视图名
        mv.setViewName("userList");

        System.out.println("pageIndex:" + pageIndex);
        System.out.println("pageSize:" + pageSize);

        return mv;
    }

    /**
     * 处理查询用户详情 /user/detail
     */
//    @RequestMapping(value = {"detail"},method = RequestMethod.POST)
    @PostMapping(value = {"detail"})
    public ModelAndView detail(@RequestParam Integer id)
            throws Exception {
        // todo:查询数据
        User user = new User(id, "tom", "汤姆");
        ModelAndView mv = new ModelAndView();
        mv.addObject("user", user);
        mv.setViewName("userDetail");
        return mv;
    }

    // 系统自动创建model对象
    @GetMapping("detail1")
    public String detail1(@RequestParam Integer id, Model model) {
        User user = new User(id, "tom", "汤姆");
        //  model.addAttribute("user",user);
        // 自动设置key为user
        model.addAttribute(user);
        // 模拟异常
        int a = 10 / 0;
        // 直接返回视图名即可
        return "userDetail";
    }

    @GetMapping("detail4")
    public String detail4(@RequestParam Integer id, Model model) throws MyException {
        try {
            User user = new User(id, "tom", "汤姆");
            //  model.addAttribute("user",user);
            // 自动设置key为user
            model.addAttribute(user);
            // 模拟异常
            int a = 10 / 0;
            // 直接返回视图名即可
            return "userDetail";
        } catch (Exception e) {
            e.printStackTrace();
            // 抛检查异常时，可以自定义错误信息
            throw new MyException(e.getMessage());
        }
    }

    /**
     * 打开新增用户界面
     */
    @GetMapping("addUser")
    public String toAddUser() {
        return "addUser";
    }

    /**
     * 保存新增用户表单
     *
     * @return
     */
    @PostMapping("addUser")
//    public String doAddUser(@ModelAttribute User user) {
    public String doAddUser(User user) {
        // 自动绑定表单元素数据到user
        System.out.println(user);
        // 暂时不跳转，只验证有没有获取到数据
        return "";
    }

    /**
     * 打开新增用户界面-页面发送的是普通的异步请求
     * 值以key-value方式发送到后台
     */
    @GetMapping("addUser1")
    public String toAddUser1() {
        return "addUser1";
    }

    @PostMapping("addUser1")
    public String doAddUser1(@RequestParam(required = false)
                                     String data) {
        System.out.println(data);
        // todo: 手动将json串转成javaBean

        return "";
    }

    /**
     * 打开新增用户界面-页面发送的是普通的异步请求
     * 值以json方式发送到后台
     */
    @GetMapping("addUser2")
    public String toAddUser2() {
        return "addUser2";
    }


    @PostMapping("addUser2")
    // @RequestBody的作用，在配置了json转换器的前提下，
    // 能自动将前端提交的json串，转为javaBean
    public String doAddUser2(@RequestBody User user) {
        System.out.println(user);
        return "";
    }

    /**
     * 处理同步表单的返回
     *
     * @param user
     * @return
     */
    @PostMapping("addUser3")
    @ResponseBody
    public ReturnData doAddUser3(User user) {
        return ReturnData.success(user, "保存成功");
    }

    /**
     * 处理异步请求的返回
     *
     * @param user
     * @return
     */
    @PostMapping("addUser4")
    @ResponseBody
    public ReturnData doAddUser4(@RequestBody User user) {
        return ReturnData.success(user, "保存成功");
    }

    @GetMapping("detail2")
    // 直接响应输出返回值给前端浏览器
    @ResponseBody
    public String detail2(@RequestParam Integer id) {
        User user = new User(id, "tom", "汤姆");
        // 返回部分数据
        return "aaa";
        // todo: 若希望将查询到的对象信息，返回给客户端？
        // 1.先通过json转换工具，将javaBean转为json串
        // 2.将json串返回即可
    }

    @GetMapping("detail3")
    // 直接响应输出返回值给前端浏览器
    @ResponseBody
    public User detail3(@RequestParam Integer id) {
        User user = new User(id, "tom", "汤姆");
        // 返回javaBean，jackson会自动
        return user;
    }

    /**
     * 局部异常处理器
     */
//    @ExceptionHandler(value = {RuntimeException.class})
    public String handleException(RuntimeException e, HttpServletRequest request) {
        // 保存错误对象
        request.setAttribute("exception", e);
        return "500";
    }
}
