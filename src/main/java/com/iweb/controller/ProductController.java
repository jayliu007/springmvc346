package com.iweb.controller;

import com.iweb.entity.Product;
import com.iweb.entity.ReturnData;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * @author jay
 * @date 2023/4/14
 * @description
 */
@Controller
@RequestMapping("product")
public class ProductController {

    @GetMapping("add")
    public String toAdd() {
        return "addProduct";
    }

    @PostMapping("add")
    @ResponseBody
    public ReturnData doAdd(HttpServletRequest request,
                            Product product,
                            @RequestParam(required = false, name = "productFile")
                                    MultipartFile file) {
        String newName = null;
        // 处理上传的图片
        if (!file.isEmpty()) {
            String path = request.getServletContext().getRealPath("/statics/file");
            File savePath = new File(path);
            // 创建文件夹
            if (!savePath.exists()) {
                savePath.mkdirs();
            }
            // 处理文件
            // 改名
            newName = UUID.randomUUID().toString().replace("-", "") + file.getOriginalFilename();
            // 创建要保存的文件对象
            File saveFile = new File(savePath, newName);
            // 保存到指定路径下的新文件名中
            try {
                file.transferTo(saveFile);
            } catch (IOException e) {
                e.printStackTrace();
                return ReturnData.error("文件保存失败");
            }
        }
        // 处理文本表单数据
        product.setFileName(newName);
        return ReturnData.success(product, "保存成功");
    }
}
