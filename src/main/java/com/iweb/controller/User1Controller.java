package com.iweb.controller;

import com.iweb.entity.User;
import org.springframework.web.HttpRequestHandler;
import org.springframework.web.servlet.mvc.HttpRequestHandlerAdapter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author jay
 * @date 2023/4/13
 * @description
 */
public class User1Controller implements HttpRequestHandler {
    @Override
    public void handleRequest(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        // 查询数据-模拟查询到的数据
        List<User> userList = new ArrayList<>();
        userList.add(new User(1, "tom1", "汤姆1"));
        userList.add(new User(2, "tom2", "汤姆2"));
        userList.add(new User(3, "tom23", "汤姆3"));
        // 原始做法，把userList保存到request中，然后转发响应到userList.jsp
        httpServletRequest.setAttribute("userList", userList);
        httpServletRequest.getRequestDispatcher("/WEB-INF/jsp/userList.jsp")
                .forward(httpServletRequest, httpServletResponse);
    }
}
