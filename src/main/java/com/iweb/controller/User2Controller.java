package com.iweb.controller;

import com.iweb.entity.User;
import org.springframework.web.HttpRequestHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author jay
 * @date 2023/4/13
 * @description
 */
public class User2Controller implements Controller {

    @Override
    public ModelAndView handleRequest(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        String id = httpServletRequest.getParameter("id");
        // todo:查询数据
        User user = new User(Integer.valueOf(id), "tom", "汤姆");
        ModelAndView mv = new ModelAndView();
        mv.addObject("user", user);
        mv.setViewName("userDetail");
        return mv;
    }
}
