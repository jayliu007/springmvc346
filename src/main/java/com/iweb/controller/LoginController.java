package com.iweb.controller;

import com.iweb.entity.ReturnData;
import com.iweb.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * @author jay
 * @date 2023/4/14
 * @description
 */
@Controller
@RequestMapping("login")
public class LoginController {
    @GetMapping("login")
    public String toLogin() {
        return "login";
    }

    @PostMapping("login")
    @ResponseBody
    public ReturnData doLogin(User user, HttpServletRequest request) {
        if ("admin".equals(user.getLoginName()) &&
                "123456".equals(user.getPassword())) {
            // 保存登录凭证
            request.getSession().setAttribute("loginUser", user);
            return ReturnData.success(null, "登录成功");
        } else {
            return ReturnData.error("登录失败，用户名或密码有误");
        }
    }
}
