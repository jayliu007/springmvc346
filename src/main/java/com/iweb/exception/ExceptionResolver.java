package com.iweb.exception;

import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author jay
 * @date 2023/4/14
 * @description 全局异常处理器
 */
public class ExceptionResolver implements HandlerExceptionResolver {
    @Override
    public ModelAndView resolveException(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) {
        MyException myException = null;
        if (e instanceof MyException) {
            myException = (MyException) e;
        } else {
            myException = new MyException("FBI ERROR WARING!!! 一个不留神，服务器摸鱼了，对于喜欢上班摸鱼的服务器，程序猿们会好好修理它。。。");
        }
        ModelAndView mv = new ModelAndView();
        mv.addObject("exception", myException);
        mv.setViewName("500");
        return mv;
    }
}
