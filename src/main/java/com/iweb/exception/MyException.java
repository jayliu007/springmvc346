package com.iweb.exception;

/**
 * @author jay
 * @date 2023/4/14
 * @description 自定义异常类，处理检查类异常
 */
public class MyException extends Exception {
    public MyException(String message) {
        super(message);
        this.message = message;
    }

    private String message;

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
