package com.iweb.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author jay
 * @date 2023/4/14
 * @description 受限资源拦截器
 */
public class AdminInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 查看是否有登录凭证
        Object loginUser = request.getSession().getAttribute("loginUser");
        if (loginUser == null) {
            // 没有登录，跳转到登录页面
            response.sendRedirect(request.getContextPath() + "/login/login");
            return false;
        }
        // 登录了，就放行请求
        return true;
    }
}
