<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>添加用户</title>
  <script src="<%=request.getContextPath()%>/statics/js/jquery-3.6.4.js"></script>
</head>
<body>
<h2>添加用户</h2>
<div>
  <p>登录名：
    <input type="text" id="loginName" name="loginName">
    <span id="check-result"></span>
  </p>
  <p>姓名：<input type="text" id="userName"><span></span></p>
  <p>性别：
    <input id="male" type="radio" name="sex" value="1" checked>男&nbsp;
    <input id="female" type="radio" name="sex" value="0">女
  </p>
  <p>身份证号：<input type="text" id="identityCode"></p>
  <p>个人邮箱：<input type="text" id="email"><span></span></p>
  <p>手机号：<input type="text" id="mobile"><span></span></p>
  <p>会员类型：
    <select name="type" id="type">
      <option value="0">普通会员</option>
      <option value="1">后台管理员</option>
    </select>
  </p>
  <p>
    <button onclick="save()">保存</button>
  </p>
</div>
<script>
    function save() {
        let user = {}; // 保存表单数据
        // 先获取表单每个元素的值
        user.loginName = $("#loginName").val()
        user.userName = $("#userName").val()
        user.mobile = $("#mobile").val()
        user.identityCode = $("#identityCode").val()
        user.email = $("#email").val()
        user.type = $("#type").val()

        $.ajax({
            url: "<%=request.getContextPath()%>/user/addUser1",
            type: "post",
            contentType: "application/x-www-form-urlencoded",
            data: "data=" + JSON.stringify(user),
            success: function (result) {
                alert("ok");
            }
        });
    }
</script>
</body>
</html>
