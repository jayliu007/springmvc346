<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>添加用户</title>
  <script src="<%=request.getContextPath()%>/statics/js/jquery-3.6.4.js"></script>
</head>
<body>
<h2>添加用户</h2>
<form action="<%=request.getContextPath()%>/user/addUser" method="post">
  <p>登录名：
    <input type="text" id="loginName" name="loginName">
    <span id="check-result"></span>
  </p>
  <p>姓名：<input type="text" name="userName"><span></span></p>
  <p>性别：
    <input type="radio" name="sex" value="1" checked>男&nbsp;
    <input type="radio" name="sex" value="0">女
  </p>
  <p>身份证号：<input type="text" name="identityCode"></p>
  <p>个人邮箱：<input type="text" name="email"><span></span></p>
  <p>手机号：<input type="text" name="mobile"><span></span></p>
  <p>出生日期：<input type="text" name="bornDate"></p>
  <p>会员类型：
    <select name="type">
      <option value="0">普通会员</option>
      <option value="1">后台管理员</option>
    </select>
  </p>
  <p>
    <button>保存</button>
  </p>
</form>
</body>
</html>
