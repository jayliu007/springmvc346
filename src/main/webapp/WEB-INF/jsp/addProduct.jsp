<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>添加商品</title>
</head>
<body>
<form action="" method="post"
      enctype="multipart/form-data">
  <p>商品名称: <input type="text" name="name"></p>
  <p>商品价格: <input type="text" name="price"></p>
  <p>商品库存: <input type="text" name="stock"></p>
  <p>商品图片: <input type="file" name="productFile"></p>
  <p>
    <button>保存</button>
  </p>
</form>
</body>
</html>
