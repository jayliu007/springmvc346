<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
  <title>用户列表</title>
</head>
<body>
<div id="main">
    <h2>用户列表</h2>
    <table>
      <tr>
        <th>序号</th>
        <th>登录名</th>
        <th>姓名</th>
      </tr>
      <c:forEach items="${requestScope.userList}"
                 var="user" varStatus="status">
        <tr>
          <td>${status.index+1}</td>
          <td>${user.loginName}</td>
          <td>${user.userName}</td>
          <td>
            <a href="">修改</a>
            <a href="">删除</a>
          </td>
        </tr>
      </c:forEach>
    </table>
  </div>
</div>
</body>
</html>
