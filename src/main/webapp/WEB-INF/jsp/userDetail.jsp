<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
  <title>用户列表</title>
</head>
<body>
<div id="main">
  <h2>用户详情</h2>
  <ul>
    <li>用户编号：${requestScope.user.id}</li>
    <li>登录名：${requestScope.user.loginName}</li>
    <li>姓名：${requestScope.user.userName}</li>
  </ul>
</div>
</div>
</body>
</html>
